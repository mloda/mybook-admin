package com.sawicka.mybook_admin.utils;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import lombok.Setter;

/**
 * Created by mloda on 05.06.17.
 */
@Setter
public class RetrieveHttpPostInfo extends AsyncTask<NameValuePair, Void, String> {

    /*params ->
        1. URL to page x,
        2. authorization header value to page x
     */
    private String requestString;
    private String header;

    @Override
    protected String doInBackground(NameValuePair... params) {
        if (params[0] != null) {
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(requestString);
            setHeader(request, header);
            try {
                List<NameValuePair> list = new ArrayList<>(params.length);
                for(int i = 0;i < params.length;i++){
                    list.add(params[i]);
                }
                request.setEntity(new UrlEncodedFormEntity(list));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            try {
                HttpResponse response = client.execute(request);
                int code = response.getStatusLine().getStatusCode();
                if (code != 200 && code != 400) {
                    return "http_status: " + code;
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                String responseValue = "";
                String temp;
                while ((temp = rd.readLine()) != null) {
                    responseValue += temp;
                }
                return responseValue;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void setHeader(HttpPost request, String param) {
        if (param != null & !param.equals("")) {
            request.addHeader("Authorization", param);
        }
    }
}
