package com.sawicka.mybook_admin.utils;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by mloda on 05.06.17.
 */

public class RetrieveHttpDeleteInfo extends AsyncTask<String, Void, String> {

    /*params ->
        1. URL to page x,
        2. authorization header value to page x
     */
    @Override
    protected String doInBackground(String... params) {
        String[] passed = params.clone();
        if(passed[0] != null) {
            HttpClient client = new DefaultHttpClient();
            HttpDelete request = new HttpDelete(passed[0]);
            setHeader(request, passed[1]);
            try {
                HttpResponse response = client.execute(request);
                int code = response.getStatusLine().getStatusCode();
                if(code != 200) {
                    return "http_status: " + code;
                }
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                String responseValue = "";
                String temp;
                while ((temp = rd.readLine()) != null) {
                    responseValue += temp;
                }
                return responseValue;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void setHeader(HttpDelete request, String param){
        if (param != null & !param.equals("")) {
            request.addHeader("Authorization", param);
        }
    }
}