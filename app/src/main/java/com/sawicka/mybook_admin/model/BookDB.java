package com.sawicka.mybook_admin.model;

import java.math.BigInteger;
import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by mloda on 01.06.17.
 */

@Getter
@Setter
@NoArgsConstructor
public class BookDB{
    private BigInteger id;
    private String name;
    private Date publicationYear;
    private BigInteger isbn;
    private Category category;
    private String publicationPlace;
    private Boolean state;
}
