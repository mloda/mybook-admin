package com.sawicka.mybook_admin.model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by mloda on 24.05.17.
 */
@Getter
@Setter
@NoArgsConstructor
public class Author {
    private int id;
    private String name;
    private String surname;

    public Author(int id, String name, String surname){
        this.id = id;
        this.name = name;
        this.surname= surname;
    }
}
