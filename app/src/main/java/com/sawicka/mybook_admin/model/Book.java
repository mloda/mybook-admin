package com.sawicka.mybook_admin.model;

import java.math.BigInteger;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by mloda on 24.05.17.
 */
@Getter
@Setter
@NoArgsConstructor
public class Book {
    private int id;
    private String name;
    private BigInteger isbn;
    private int publicationYear;
    private String publicationPlace;
    private int state;
    private Category category;

    public Book(int id, String name, BigInteger isbn, int publicationYear, String publicationPlace,
                int state, Category category){
        this.id = id;
        this.name = name;
        this.isbn = isbn;
        this.publicationYear = publicationYear;
        this.publicationPlace = publicationPlace;
        this.state = state;
        this.category = category;
    }
}
