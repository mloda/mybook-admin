package com.sawicka.mybook_admin.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by mloda on 24.05.17.
 */
@Getter
@Setter
@NoArgsConstructor
public class Category {
    private int id;
    private String name;

    public Category(int id, String name){
        this.id = id;
        this.name = name;
    }
}
