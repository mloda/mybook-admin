package com.sawicka.mybook_admin.model;

import java.sql.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by mloda on 27.05.17.
 */
@Setter
@Getter
@NoArgsConstructor
public class Log {
    private int id;
    private Date date;
    private String description;

    public Log(int id, Date date, String description){
        this.id = id;
        this.date = date;
        this.description = description;
    }
}
