package com.sawicka.mybook_admin.model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by mloda on 03.06.17.
 */
@Getter
@Setter
public class BookDBStringVal implements Parcelable{
    private String id;
    private String name;
    private String publicationYear;
    private String isbn;
    private String category;
    private String publicationPlace;
    private String state;

    public BookDBStringVal(BookDB b){
        this.id = b.getId().toString();
        this.name = b.getName();
        this.publicationYear = b.getPublicationYear().toString();
        this.isbn = b.getIsbn().toString();
        this.category = b.getCategory().getName();
        this.publicationPlace = b.getPublicationPlace();
        this.state = b.getState() ? "dostępna" : "niedostępna";
    }

    protected BookDBStringVal(Parcel in) {
        id = in.readString();
        name = in.readString();
        publicationYear = in.readString();
        isbn = in.readString();
        category = in.readString();
        publicationPlace = in.readString();
        state = in.readString();
    }

    public static final Creator<BookDBStringVal> CREATOR = new Creator<BookDBStringVal>() {
        @Override
        public BookDBStringVal createFromParcel(Parcel in) {
            return new BookDBStringVal(in);
        }

        @Override
        public BookDBStringVal[] newArray(int size) {
            return new BookDBStringVal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(publicationYear);
        dest.writeString(isbn);
        dest.writeString(category);
        dest.writeString(publicationPlace);
        dest.writeString(state);
    }
}
