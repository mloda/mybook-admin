package com.sawicka.mybook_admin.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sawicka.mybook_admin.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mloda on 27.05.17.
 */

public class LogHistoryListAdapter extends BaseAdapter {
    public ArrayList<? extends HashMap<String, String>> list;
    private Activity activity;
    private TextView date;
    private TextView description;

    public LogHistoryListAdapter(Activity activity, ArrayList<? extends HashMap<String, String>> list){
        super();
        this.activity = activity;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=activity.getLayoutInflater();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.log_history_row_layout, null);
            date = (TextView) convertView.findViewById(R.id.date);
            description = (TextView) convertView.findViewById(R.id.description);
        }

        HashMap<String, String> map=list.get(position);
        date.setText(map.get("date"));
        description.setText(map.get("desc"));

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
