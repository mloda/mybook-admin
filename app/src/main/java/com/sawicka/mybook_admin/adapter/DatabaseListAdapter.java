package com.sawicka.mybook_admin.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sawicka.mybook_admin.activity.database.DatabaseAddItemActivity;
import com.sawicka.mybook_admin.activity.database.DatabaseItemDetailsActivity;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.BookDBStringVal;
import com.sawicka.mybook_admin.utils.RetrieveHttpDeleteInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import lombok.Getter;

/**
 * Created by mloda on 31.05.17.
 */
@Getter
public class DatabaseListAdapter extends BaseAdapter {
    private ArrayList<? extends HashMap<String, String>> list;
    private ArrayList<? extends BookDBStringVal> books;
    private Activity activity;
    private TextView name;
    private ImageButton detailsButton;
    private ImageButton editButton;
    private ImageButton deleteButton;

    public DatabaseListAdapter(Activity activity, ArrayList<? extends HashMap<String, String>> list,
                               ArrayList<? extends BookDBStringVal> books){
        super();
        this.activity = activity;
        this.list = list;
        this.books = books;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();

        if(convertView == null){
            convertView = inflater.inflate(R.layout.database_item_row_layout, null);
            name = (TextView) convertView.findViewById(R.id.name);
            detailsButton = (ImageButton) convertView.findViewById(R.id.details_button);
            editButton = (ImageButton) convertView.findViewById(R.id.edit_button);
            deleteButton = (ImageButton) convertView.findViewById(R.id.delete_button);
        }

        HashMap<String, String> map = list.get(position);
        name.setText(map.get("name"));

        detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = list.get(position).get("id");
                String name = list.get(position).get("name");
                startIntentWithItem(getBookWith(id, name));
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, DatabaseAddItemActivity.class);
                String id = list.get(position).get("id");
                String name = list.get(position).get("name");
                intent.putExtra("BOOK", getBookWith(id, name));
                activity.startActivity(intent);

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity.getApplicationContext(), list.get(position).get("name"), Toast.LENGTH_SHORT).show();
                String responseString = "";
                try {
                    responseString = new RetrieveHttpDeleteInfo()
                            .execute("https://pacific-tundra-96657.herokuapp.com/books/" + list.get(position).get("name"), "db").get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                /*if (responseString.startsWith("http_status:")) {
                    Toast.makeText(activity.getApplicationContext(), "Got: " + responseString, Toast.LENGTH_SHORT).show();
                } else */
                Toast.makeText(activity.getApplicationContext(), "Deleted!", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private void startIntentWithItem(BookDBStringVal book){
        Intent intent = new Intent(activity, DatabaseItemDetailsActivity.class);
        intent.putExtra("BOOK", book);
        activity.startActivity(intent);
    }

    private BookDBStringVal getBookWith(String id, String name){
        for(BookDBStringVal b : this.books){
            if(b.getId().equals(id) && b.getName().equals(name))
                return b;
        }
        return null;
    }
}
