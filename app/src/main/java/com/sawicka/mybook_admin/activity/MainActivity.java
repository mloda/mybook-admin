package com.sawicka.mybook_admin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.sawicka.mybook_admin.activity.database.DatabaseActivity;
import com.sawicka.mybook_admin.utils.ImageLoadTask;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.presenter.AuthPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    private AuthPresenter authPresenter = new AuthPresenter();
    private String userName;

    private static final int SIGN_IN = 1;
    private static final int RESOLVE_CONNECTION = 2; //trying to connect again

    @BindView(R.id.logged_user_name) TextView user_name_value;
    @BindView(R.id.user_image) ImageView user_image;
    @BindView(R.id.logging_button) Button logging_Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        authPresenter.setUpClient(this, getApplicationContext());
    }

    @OnClick(R.id.logging_button)
    public void logging(){
        if (userName == null) {
            authPresenter.clientConnect();
            startActivityForResult(authPresenter.startSignInActivity(), SIGN_IN);
        } else {
            authPresenter.clientDisconnect();
            user_name_value.setText("");
            userName = null;
            user_image.setImageBitmap(null);
            logging_Button.setText("Zaloguj");
        }
    }

    @OnClick(R.id.db_button)
    public void database(){
        Intent intent = new Intent(this, DatabaseActivity.class);
        //intent.putExtra();
        startActivity(intent);
    }

    @OnClick(R.id.return_book_button)
    public void returnBook(){
        Intent intent = new Intent(this, ReturnBookActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.activity_log_button)
    public void activityLogHistory(){
        Intent intent = new Intent(this, LogHistoryMainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.statistics_button)
    public void statistics(){
        Intent intent = new Intent(this, StatisticsActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESOLVE_CONNECTION && resultCode == RESULT_OK) {
            authPresenter.clientConnect();
        } else if (requestCode == SIGN_IN) {
            GoogleSignInAccount acct = authPresenter.getClientAuthorized(data);
            if (acct != null) {
                user_name_value.setText("Witaj, " + acct.getDisplayName() + "!");
                userName = acct.getDisplayName();
                if(acct.getPhotoUrl() != null)
                    new ImageLoadTask(acct.getPhotoUrl().toString(), user_image).execute();
                logging_Button.setText("Wyloguj");
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}
}
