package com.sawicka.mybook_admin.activity.database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.adapter.DatabaseListAdapter;
import com.sawicka.mybook_admin.model.BookDBStringVal;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DatabaseListActivity extends Activity {
    private ArrayList<? extends HashMap<String, String>> list;
    private ArrayList<? extends BookDBStringVal> books;
    @BindView(R.id.database_list_item_list_view) ListView databaseListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_list);
        ButterKnife.bind(this);

        this.list = getIntent().getParcelableArrayListExtra("LIST");
        this.books = getIntent().getParcelableArrayListExtra("BOOKS");
        DatabaseListAdapter adapter = new DatabaseListAdapter(this, list, books);
        databaseListView.setAdapter(adapter);
    }

    @OnClick(R.id.new_book_add)
    public void addNewBook(){
        Intent intent = new Intent(this, DatabaseAddItemActivity.class);
        startActivity(intent);
    }
}
