package com.sawicka.mybook_admin.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.Author;
import com.sawicka.mybook_admin.model.Book;
import com.sawicka.mybook_admin.model.Category;
import com.sawicka.mybook_admin.utils.RetrieveHttpGetInfo;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatisticsActivity extends Activity {
    private List<Author> authors;
    private List<Book> books;
    private List<Category> categories;
    private RetrieveHttpGetInfo infoAuthor = new RetrieveHttpGetInfo();
    private RetrieveHttpGetInfo infoBook = new RetrieveHttpGetInfo();
    private RetrieveHttpGetInfo infoCategory = new RetrieveHttpGetInfo();

    @BindView(R.id.most_popular_authors) ListView mostPopularAuthors;
    @BindView(R.id.most_popular_books) ListView mostPopularBooks;
    @BindView(R.id.most_popular_categories) ListView mostPopularCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        ButterKnife.bind(this);

        try {
            retrieveStatistics();
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        setRetrievedStatistics();
    }

    private void retrieveStatistics() throws ExecutionException, InterruptedException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        String responseStringAuthors = infoAuthor.execute("https://rsilog.herokuapp.com/rsi/stat/mostPopular/authors", "log").get();
        if(responseStringAuthors.startsWith("http_status:")) {
            statusErrorThrow();
        } else {
            this.authors = Arrays.asList(mapper.readValue(responseStringAuthors, Author[].class));
        }

        String responseStringBooks = infoBook.execute("https://rsilog.herokuapp.com/rsi/stat/mostPopular/books", "log").get();
        if(responseStringBooks.startsWith("http_status:")) {
            statusErrorThrow();
        } else {
            this.books = Arrays.asList(mapper.readValue(responseStringBooks, Book[].class));
        }

        String responseStringCategories = infoCategory.execute("https://rsilog.herokuapp.com/rsi/stat/mostPopular/categories", "log").get();
        if(responseStringCategories.startsWith("http_status:")) {
            statusErrorThrow();
        } else {
            this.categories = Arrays.asList(mapper.readValue(responseStringCategories, Category[].class));
        }
    }

    private void statusErrorThrow(){
        //TODO: wyrzuć błąd na podstawie statusu http
    }

    private void setRetrievedStatistics(){
        List<String> authorNameSurname = setAuthorNameSurnameList();
        List<String> bookName = setBookNameList();
        List<String> categoryName = setCategoryNameList();
        mostPopularAuthors.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, authorNameSurname));
        mostPopularBooks.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, bookName));
        mostPopularCategories.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categoryName));
    }

    private LinkedList<String> setBookNameList(){
        LinkedList<String> bookNames = new LinkedList<>();
        for(Book book : this.books){
            bookNames.add(book.getName());
        }
        return bookNames;
    }

    private LinkedList<String> setAuthorNameSurnameList(){
        LinkedList<String> authorNameSurname = new LinkedList<>();
        for(Author author : this.authors){
            authorNameSurname.add(author.getName() + " " + author.getSurname());
        }
        return authorNameSurname;
    }

    private LinkedList<String> setCategoryNameList(){
        LinkedList<String> categoryName = new LinkedList<>();
        for(Category category : this.categories){
            categoryName.add(category.getName());
        }
        return categoryName;
    }
}
