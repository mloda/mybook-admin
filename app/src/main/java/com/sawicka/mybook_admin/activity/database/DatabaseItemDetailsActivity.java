package com.sawicka.mybook_admin.activity.database;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.BookDBStringVal;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatabaseItemDetailsActivity extends AppCompatActivity {
    private BookDBStringVal book;

    @BindView(R.id.database_item_details_name) TextView name;
    @BindView(R.id.database_item_details_isbn) TextView isbn;
    @BindView(R.id.database_item_details_category) TextView category;
    @BindView(R.id.database_item_details_publication_year) TextView year;
    @BindView(R.id.database_item_details_publication_place) TextView place;
    @BindView(R.id.database_item_details_state) TextView state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_item_details);

        this.book = getIntent().getParcelableExtra("BOOK");
        ButterKnife.bind(this);

        name.setText(book.getName());
        isbn.setText(book.getIsbn());
        category.setText(book.getCategory());
        year.setText(book.getPublicationYear());
        place.setText(book.getPublicationPlace());
        state.setText(book.getState());
    }
}
