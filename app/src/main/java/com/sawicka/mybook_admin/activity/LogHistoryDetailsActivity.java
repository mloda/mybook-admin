package com.sawicka.mybook_admin.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.adapter.LogHistoryListAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mloda on 27.05.17.
 */

public class LogHistoryDetailsActivity extends Activity {

    private ArrayList<? extends HashMap<String, String>> list;
    @BindView(R.id.log_details_list_view) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_history_details);
        ButterKnife.bind(this);

        this.list = getIntent().getParcelableArrayListExtra("LIST");
        LogHistoryListAdapter adapter = new LogHistoryListAdapter(this, list);
        listView.setAdapter(adapter);
    }
}
