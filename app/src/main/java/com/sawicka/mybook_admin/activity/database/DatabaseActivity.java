package com.sawicka.mybook_admin.activity.database;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.BookDB;
import com.sawicka.mybook_admin.model.BookDBStringVal;
import com.sawicka.mybook_admin.utils.RetrieveHttpGetInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class DatabaseActivity extends Activity {
    private ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.database_authors_button)
    public void openDatabaseAuthors(){
        ArrayList<HashMap<String, String>> list= new ArrayList<>();
//        startIntentWithList(list);
    }

    @OnClick(R.id.database_books_button)
    public void openDatabaseBooks(){
        ArrayList<HashMap<String, String>> list;
        ArrayList<BookDB> booksDb = new ArrayList<>();
        String responseString = "";
        try {
            responseString = new RetrieveHttpGetInfo()
                    .execute("https://pacific-tundra-96657.herokuapp.com/books", "db").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(responseString.startsWith("http_status: ")) {
            Toast.makeText(getApplicationContext(), "Got: " + responseString, Toast.LENGTH_SHORT).show();
        } else {
            try {
                List<BookDB> temp = Arrays.asList(mapper.readValue(responseString, BookDB[].class));
                booksDb = new ArrayList<>(temp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        list = getBookListMap(booksDb);
        startIntentWithList(list, getBooksStringValFrom(booksDb));
    }

    @OnClick(R.id.database_categories_button)
    public void openDatabaseCategories(){
        ArrayList<HashMap<String, String>> list= new ArrayList<>();
//        startIntentWithList(list, );
    }

    private void startIntentWithList(ArrayList<HashMap<String, String>> list,
                                     ArrayList<BookDBStringVal> books){
        Intent intent = new Intent(this, DatabaseListActivity.class);
        intent.putExtra("LIST", list);
        intent.putExtra("BOOKS", books);
        startActivity(intent);
    }

    private ArrayList<HashMap<String, String>> getBookListMap(List<BookDB> booksDb){
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        for(BookDB book : booksDb){
            HashMap<String, String> item = new HashMap<>();
            item.put("name", book.getName());
            item.put("id", book.getId().toString());
            list.add(item);
        }
        return list;
    }

    private ArrayList<BookDBStringVal> getBooksStringValFrom(List<BookDB> booksDb){
        ArrayList<BookDBStringVal> books = new ArrayList<>();
        for(BookDB b : booksDb){
            BookDBStringVal book = new BookDBStringVal(b);
            books.add(book);
        }
        return books;
    }
}
