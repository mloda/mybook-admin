package com.sawicka.mybook_admin.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sawicka.mybook_admin.R;

public class ReturnBookActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_book);
    }
}
