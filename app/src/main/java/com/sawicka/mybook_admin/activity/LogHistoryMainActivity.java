package com.sawicka.mybook_admin.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.Log;
import com.sawicka.mybook_admin.utils.RetrieveHttpGetInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogHistoryMainActivity extends AppCompatActivity {

    public static final String DATE = "date";
    public static final String DESC = "desc";

    private ObjectMapper mapper = new ObjectMapper();
    private List<Log> logs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_history_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.authors_log_button)
    public void authorsLogButtonHandle(){
        try {
            retrieveLogHistory("https://rsilog.herokuapp.com/rsi/log/authors/history", "log");
        } catch (ExecutionException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        startIntentWithList();
    }

    @OnClick(R.id.books_log_button)
    public void booksLogButtonHandle(){
        try {
            retrieveLogHistory("https://rsilog.herokuapp.com/rsi/log/books/history", "log");
        } catch (ExecutionException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        startIntentWithList();
    }

    @OnClick(R.id.categories_log_button)
    public void categoriesLogButtonHandle(){
        try {
            retrieveLogHistory("https://rsilog.herokuapp.com/rsi/log/categories/history", "log");
        } catch (ExecutionException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        startIntentWithList();
    }

    private void retrieveLogHistory(String url, String authString) throws ExecutionException, InterruptedException, IOException {
        String responseStringAuthors = new RetrieveHttpGetInfo().execute(url, authString).get();
        if(responseStringAuthors.startsWith("http_status:")) {
            // TODO: statusErrorThrow
        } else {
            this.logs = Arrays.asList(mapper.readValue(responseStringAuthors, Log[].class));
        }
    }

    private void startIntentWithList(){
        Intent intent = new Intent(this, LogHistoryDetailsActivity.class);
        intent.putExtra("LIST", getArrayWithLogs());
        startActivity(intent);
    }

    private ArrayList<HashMap<String, String>> getArrayWithLogs(){
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        for(Log log : this.logs){
            HashMap<String, String> logItem = new HashMap<>();
            logItem.put(DATE, log.getDate().toString());
            logItem.put(DESC, log.getDescription());
            list.add(logItem);
        }
        return list;
    }
}
