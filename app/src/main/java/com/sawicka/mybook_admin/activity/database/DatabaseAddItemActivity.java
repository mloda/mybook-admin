package com.sawicka.mybook_admin.activity.database;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sawicka.mybook_admin.R;
import com.sawicka.mybook_admin.model.Author;
import com.sawicka.mybook_admin.model.BookDB;
import com.sawicka.mybook_admin.model.BookDBStringVal;
import com.sawicka.mybook_admin.model.Category;
import com.sawicka.mybook_admin.utils.RetrieveHttpGetInfo;
import com.sawicka.mybook_admin.utils.RetrieveHttpPostInfo;

import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DatabaseAddItemActivity extends AppCompatActivity {

    private ObjectMapper mapper = new ObjectMapper();
    private List<Category> categories;
    private List<Author> authors;
    private List<String> categoryNames = new ArrayList<>();
    private List<String> authorNames = new ArrayList<>();
    private Activity activityList;
    private BookDBStringVal oldBookValue;

    @BindView(R.id.book_new_name) EditText newName;
    @BindView(R.id.book_new_author) MultiAutoCompleteTextView newAuthor;
    @BindView(R.id.book_new_isbn) EditText newIsbn;
    @BindView(R.id.book_new_category) Spinner newCategory;
    @BindView(R.id.book_new_publication_year) EditText newPublicationYear;
    @BindView(R.id.book_new_publication_place) EditText newPublicationPlace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_add_item);
        ButterKnife.bind(this);

        setCategories();
        setCategoriesNames();
        setAuthors();
        setAuthorsNames();

        this.oldBookValue = getIntent().getParcelableExtra("BOOK");
        if(this.oldBookValue != null){
            newName.setText(oldBookValue.getName());
            newIsbn.setText(oldBookValue.getIsbn().toString());
            newCategory.setSelection(categories.indexOf(getCategoryBy(oldBookValue.getCategory())));
            Date date = new Date(oldBookValue.getPublicationYear());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            newPublicationYear.setText(calendar.get(Calendar.YEAR) + "-" +
                    calendar.get(Calendar.MONTH) +"-" + calendar.get(Calendar.DAY_OF_MONTH));
            newPublicationPlace.setText(oldBookValue.getPublicationPlace());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, this.categoryNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        newCategory.setAdapter(adapter);

        ArrayAdapter<String> aaStr = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, authorNames);
        newAuthor.setAdapter(aaStr);
        newAuthor.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer() );
    }

    @OnClick(R.id.book_new_save_button)
    public void saveNewBook(){
        BookDB book = new BookDB();
        if(newName.getText() != null && newAuthor.getText() != null && newIsbn.getText() != null &&
                newCategory.getSelectedItem() != null && newPublicationPlace.getText() != null &&
                newPublicationYear.getText() != null) {
            book.setName(newName.getText().toString());
            book.setIsbn(new BigInteger(newIsbn.getText().toString()));
            book.setCategory(getCategoryBy(newCategory.getSelectedItem().toString()));
            //TODO: check if every author exists

            book.setPublicationPlace(newPublicationPlace.getText().toString());
            String dateValuesTemp = newPublicationYear.getText().toString();
            String[] dateValues = dateValuesTemp.split("-");
            save(book, dateValues);
        } else{
            Toast.makeText(getApplicationContext(), "Podaj wszystkie wartości.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void save(BookDB book, String[] dateValues){
        String responseString = "";
        RetrieveHttpPostInfo post = new RetrieveHttpPostInfo();
        post.setRequestString("https://pacific-tundra-96657.herokuapp.com/books");
        post.setHeader("db");
        try {
            /*https://pacific-tundra-96657.herokuapp.com/books?authors[]=Nick Vujicic&name=name&
            year=2017&month=1&dayOfMonth=1&
            isbn=12121212&categoryName=popular&publicationPlace=Warsaw*/
            String authorsAll = newAuthor.getText().toString();
            responseString = post.execute(
                    new BasicNameValuePair("authors[]", authorsAll.substring(0,authorsAll.length()-2)),
                    new BasicNameValuePair("name", book.getName()),
                    new BasicNameValuePair("year", dateValues[0]),
                    new BasicNameValuePair("month", dateValues[1]),
                    new BasicNameValuePair("dayOfMonth", dateValues[2]),
                    new BasicNameValuePair("isbn", book.getIsbn().toString()),
                    new BasicNameValuePair("categoryName", book.getCategory().getName()),
                    new BasicNameValuePair("publicationPlace", book.getPublicationPlace())
            ).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(responseString.startsWith("http_status:")) {
            Toast.makeText(getApplicationContext(), "Got: " + responseString, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Saved!" + responseString, Toast.LENGTH_SHORT).show();
        }
        this.finish();

    }

    private void setCategories(){
        String responseString = "";
        try {
            responseString = new RetrieveHttpGetInfo()
                    .execute("https://pacific-tundra-96657.herokuapp.com/categories", "db").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(responseString.startsWith("http_status:")) {
            Toast.makeText(getApplicationContext(), "Got: " + responseString, Toast.LENGTH_SHORT).show();
        } else {
            try {
                this.categories = Arrays.asList(mapper.readValue(responseString, Category[].class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setAuthors(){
        String responseString = "";
        try {
            responseString = new RetrieveHttpGetInfo()
                    .execute("https://pacific-tundra-96657.herokuapp.com/authors", "db").get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(responseString.startsWith("http_status:")) {
            Toast.makeText(getApplicationContext(), "Got: " + responseString, Toast.LENGTH_SHORT).show();
        } else {
            try {
                this.authors = Arrays.asList(mapper.readValue(responseString, Author[].class));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setCategoriesNames(){
        for(Category category : categories){
            categoryNames.add(category.getName());
        }
    }

    private void setAuthorsNames(){
        for(Author author : authors){
            authorNames.add(author.getName() + " " + author.getSurname());
        }
    }

    private Category getCategoryBy(String name){
        for(Category category : categories){
            if(category.getName().equals(name))
                return category;
        }
        return null;
    }
}
